# Elasticsearch Cluster Deployment on Docker Swarm

This deploys 3 node elasticsearch 5.2.2 cluster on a docker swarm cluster. The es docker images comes from 
secopstech docker hub repo. This image actually forked from the official elasticsearch repo but it doesn't 
contains x-pack.

## Requirements

Since each elastic node should be scheduled to specific swarm workers for data persistency, you neeed to have 
a swarm cluster with at least 3 swarm worker. Compose creates a named volume on each worker host for elastic files.


## How to
First you need to set appropriate vm.max_map_count and vm.swappiness values on all your workers:

```
vm.max_map_count=262144
vm.swappiness=0
```

Then grab the elastic-stack.yml, change the limits values as you needs and run following command on your on of your 
swam manager:

```
# docker stack deploy -c elastic-stack.yml my-elasticsearch-cluster
```

Within a minute an elasticsearch cluster should be ready. You can access the es api from every workers' IP address.
That's all.